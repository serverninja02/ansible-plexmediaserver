Ansible-plexmediaserver
============
This is my ansible repo building out my plex media server

Requirements:
-------------
 - Ansible 2.1.x or greater
 - Target machine running either Ubuntu 14.04 or CentOS 7 build 1511
Hosts:
------
The "hosts" file contains the hosts

Roles:
------
 - nfs-client - Configures nfsutils and an nfs mount for Plex. I use NFS to mount to a nfs share on the hypervisor hosting the VM for Plex. So in order for the plex server to be disposable without hassle, I use an NFS mount to keep the data and media files off the plex server (This is optional).
 - plex - Builds and configures a plex media server. Places storage of plex database in `/storage/plex` (can override with group vars)

Usage:
======
 - Prepare a host that has 1GB or more of RAM with a minimal install of CentOS 7
 - Edit the `./hosts` file
 - Run the following to deploy machine state with ansible:

**NOTE: This assumes you have centos user with passwordless sudo**
```
ansible-playbook -u centos -k -b -i production site.yml
```

Vagrant:
========
Vagrant can be used to build the plex media server (without the nfs-clients role). Currently the `Vagrantfile` is configured to use parallels. However it can be easily adjusted for Virtualbox. Just run `vagrant up`. Also `vagrant provision` to re-run ansible.

License:
========
Copyright [2016] [John W. Reed II]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
